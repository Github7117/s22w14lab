package dtos;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor

public class VowelCountDTO {
    private int aCount;
    private int eCount;
    private int iCount;
    private int oCount;
    private int uCount;
}
