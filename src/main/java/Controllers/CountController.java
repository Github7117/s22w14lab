package Controllers;

import dtos.VowelCountDTO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Locale;

@RestController
public class CountController {

    @GetMapping (path = "/count")
    public VowelCountDTO countVowels(@RequestParam(name = "word") String word){
        VowelCountDTO dto = new VowelCountDTO();

        word = word.toLowerCase();

        for (int i = 0; i < word.length(); i++) {

            char c = word.charAt(i);

            switch (c){
                case 'a':
                    dto.setACount(dto.getACount() + 1);
                case 'e':
                    dto.setACount(dto.getECount() + 1);
                case 'i':
                    dto.setACount(dto.getICount() + 1);
                case 'o':
                    dto.setACount(dto.getOCount() + 1);
                case 'u':
                    dto.setACount(dto.getUCount() + 1);
            }

        }

        return dto;
    }
}
