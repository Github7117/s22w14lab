package edu.bd.cs.qwn5005.w14lab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class W14LabApplication {

    public static void main(String[] args) {
        SpringApplication.run(W14LabApplication.class, args);
    }

}
